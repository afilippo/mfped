package ru.afilippo;

import org.jfree.data.xy.XYSeries;

abstract class ValueDependency {
    protected PairValues series;
    protected String title;
    abstract PairValues getDependency();

    String getTitle(){
        return title;
    }
}
