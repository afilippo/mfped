package ru.afilippo;

import org.jfree.data.xy.XYSeries;

class RandomDependency extends ValueDependency{
    private double a;
    private int count;

    RandomDependency(String title){
        this.title = title;
    }

    void SetParameters(int count, double a){
        this.count = count;
        this.a = a;
    }

    @Override
    PairValues getDependency() {
        series = new PairValues();

        for (double x = 0; x < count; x++){
            double y = a * (RandomGenerate.GetDouble() - 0.5);
            series.add(x, y);
        }

        return series;
    }
}