package ru.afilippo;

import java.awt.event.ItemEvent;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

public class SpectrRandWindow extends JPanel {
    private GraphicPan graphicPan;
    private SpectrRandDependencyCreator dependencyCreator;

    SpectrRandWindow(){
        dependencyCreator = new SpectrRandDependencyCreator(1000);
        graphicPan = new GraphicPan("spectrerand", 700, 430);
        graphicPan.setValues(dependencyCreator.getValues());
        this.add(graphicPan);
    }
}
