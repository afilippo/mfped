package ru.afilippo;

public class FunctionDependencyCreator {
    UserFunction userFunction;

    FunctionDependencyCreator(UserFunction userFunction){
        this.userFunction = userFunction;
    }

    public PairValues getValues(double a, double b, double from, double to, double step){
        PairValues storage = new PairValues();
        for (double x = from; x < to; x += step){
            storage.add(x, userFunction.func(a, b, x));
        }

        return storage;
    }

    interface UserFunction{
        double func(double a, double b, double x);
    }
}
