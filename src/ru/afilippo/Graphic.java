package ru.afilippo;

//import javax.swing.JFrame;
import java.awt.Dimension;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class Graphic extends JPanel {
    private static final int DEFAULT_WIDTH = 350;
    private static final int DEFAULT_HEIGHT = 200;

    private String title;
    private int width;
    private int height;

    private ChartPanel chartPanel;

    private boolean smoothed = false;

    Graphic(String title, int width, int height) {
        this.width = width;
        this.height = height;
        this.title = title;
    }

    Graphic(String title) {
        this(title, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    void setValues(PairValues pairValues){
        XYDataset xyDataset = new XYSeriesCollection(pairValues.convertToSeries());
        JFreeChart chart = ChartFactory
                .createXYLineChart(title, "x", "y",
                        xyDataset,
                        PlotOrientation.VERTICAL,
                        false, true, true);

        if (chartPanel == null){
            chartPanel = new ChartPanel(chart);
            chartPanel.setPreferredSize(new Dimension(width, height));
            this.add(chartPanel);
        } else {
            chartPanel.setChart(chart);
        }

        if (smoothed){
            makeSmoothed();
        }
    }

    void setSmoothed(){
        smoothed = true;
        makeSmoothed();
    }

    private void makeSmoothed(){
        chartPanel.getChart().getXYPlot().setRenderer(new XYSplineRenderer());
    }
}
