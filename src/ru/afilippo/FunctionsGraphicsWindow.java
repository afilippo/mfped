package ru.afilippo;

import java.awt.GridLayout;

import javax.swing.JPanel;

class FunctionsGraphicsWindow extends JPanel {

    FunctionsGraphicsWindow(){
        this.setLayout(new GridLayout(2,2));

//        FunctionDependencyCreator linearDependency = new FunctionDependencyCreator();
//        PairValues linearValues = linearDependency.getValues(4, 10, 0, 50, 0.1);
//        GraphicPan linearPanel = new GraphicPan("Linear", linearValues);
//        linearPanel.setInputs("a", "b");
//        linearPanel.setInputCallback((strings) -> {
//            double a, b;
//
//            try {
//                a = Double.parseDouble(strings[0]);
//            } catch (NumberFormatException e1){
//                a = 0;
//            }
//
//            try {
//                b = Double.parseDouble(strings[1]);
//            } catch (NumberFormatException e2){
//                b = 0;
//            }
//            return linearDependency.getValues(a, b, 0, 50, 0.1);
//        });
//        this.add(linearPanel);

        FunctionGraphicPan linear = new FunctionGraphicPan("Linear", (a, b, x) -> (a * x + b));
        linear.setDefaultParams(4, 10);
        linear.setInterval(0, 50, 0.1);
        linear.Calculate();
        this.add(linear);

        FunctionGraphicPan reverseLinear = new FunctionGraphicPan("Reverse linear", (a, b, x) -> (-a * x + b));
        reverseLinear.setDefaultParams(4, 10);
        reverseLinear.setInterval(0, 50, 0.1);
        reverseLinear.Calculate();
        this.add(reverseLinear);

        FunctionGraphicPan exp = new FunctionGraphicPan("Exponent", (a, b, x) -> (b * Math.exp(a * x)));
        exp.setDefaultParams(4, 10);
        exp.setInterval(0, 4, 0.01);
        exp.Calculate();
        this.add(exp);

        FunctionGraphicPan reverseExp = new FunctionGraphicPan("Reverse exponent", (a, b, x) -> (b * Math.exp(-a * x)));
        reverseExp.setDefaultParams(4, 10);
        reverseExp.setInterval(0, 4, 0.01);
        reverseExp.Calculate();
        this.add(reverseExp);

//
//        RandomDependency randomDependency = new RandomDependency("Random");
//        RandomGraphicPan pgRandom = new RandomGraphicPan(randomDependency);
//        randomDependency.SetParameters(100, 50);
//        this.add(pgRandom);
//        pgRandom.Calculate();
    }
}
