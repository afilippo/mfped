package ru.afilippo;

public class SpectrRandDependencyCreator {
    private int N;

    public SpectrRandDependencyCreator(int n) {
        N = n;
    }

    public PairValues getValues(){
        PairValues output = new PairValues();
        double[] Re = new double[N];
        double[] Im = new double[N];
        double[] Cn = new double[N];
        int S = 1;

        double[] massXY = new double[N];
        for (int i = 0; i < N; i++){
            massXY[i] = RandomGenerate.GetDouble();
        }



        double yMax = getMax(massXY);
        double yMin = getMin(massXY);
        for (int i = 0; i < N; i++) {
            massXY[i] = ((massXY[i] - yMin) / (yMax - yMin) - 0.5) * 2 * S;
        }

        int constant = 1;


        for (int i = 0; i < massXY.length; i++){
            massXY[i] = massXY[i] + constant;
        }

        for (int n = 0; n < N; n++){
            for (int k = 0; k < N; k++){
                Re[n] += massXY[k] * Math.cos((2 * Math.PI * n * k) / N);
                Im[n] += massXY[k] * Math.sin((2 * Math.PI * n * k) / N);
            }
            Re[n] = Re[n] / N;
            Im[n] = Im[n] / N;
            Cn[n] = Math.sqrt(Math.pow(Re[n], 2) + Math.pow(Im[n], 2));

        }

        double dt1 = 0.001;
        double fgr = 1 / (2 * dt1);
        double df = 2 * fgr / N;
        int k1 = 0;
        for (double n = 0; n < N / 2; n += df){
            output.add(n, Cn[k1]);
            k1++;
        }
        return output;
    }

    private double getMax(double[] array){
        double max = array[0];
        for (int i = 1; i < array.length; i++){
            if (array[i] > max){
                max = array[i];
            }
        }
        return max;
    }

    private double getMin(double[] array){
        double min = array[0];
        for (int i = 1; i < array.length; i++){
            if (array[i] < min){
                min = array[i];
            }
        }
        return min;
    }
}
