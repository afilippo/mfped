package ru.afilippo;

import java.awt.event.ItemEvent;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class CardioWindow extends JPanel {
    private GraphicPan cardioPan;

    private JRadioButton checkboxTicks;
    private JRadioButton checkboxBeat;
    private JRadioButton checkboxPulse;

    enum SelectedVariant {
        ticks,
        beat,
        pulse
    }

    private SelectedVariant selected;

    private CardioDependencyCreator dependencyCreator;

    private double a;
    private double f0;
    private double dt;

    CardioWindow() {
        a = 45;
        f0 = 14;
        dt = 0.001;

        selected = SelectedVariant.ticks;
        checkboxTicks = new JRadioButton("Тики", true);
        checkboxBeat = new JRadioButton("Удар", false);
        checkboxPulse = new JRadioButton("Пульс", false);

        dependencyCreator = new CardioDependencyCreator(1000);
        cardioPan = new GraphicPan("Импульсы", 700, 430);

        cardioPan.setValues(getValue());

        cardioPan.setInputs("A", "f0", "dt");
        cardioPan.setInputsValues(a, f0, dt);
        cardioPan.setInputCallback((defaultNumber, strings) -> {
            a = defaultNumber.getDouble(strings[0]);
            f0 = defaultNumber.getDouble(strings[1]);
            dt = defaultNumber.getDouble(strings[2]);
            return getValue();
        });

        checkboxBeat.addItemListener(itemListener -> {
            if (itemListener.getStateChange() == ItemEvent.SELECTED){
                selected = SelectedVariant.beat;
                cardioPan.setValues(getValue());
            }
        });

        checkboxPulse.addItemListener(itemListener -> {
            if (itemListener.getStateChange() == ItemEvent.SELECTED){
                selected = SelectedVariant.pulse;
                cardioPan.setValues(getValue());
            }
        });

        checkboxTicks.addItemListener(itemListener -> {
            if (itemListener.getStateChange() == ItemEvent.SELECTED){
                selected = SelectedVariant.ticks;
                cardioPan.setValues(getValue());
            }
        });

        ButtonGroup group = new ButtonGroup();
        group.add(checkboxBeat);
        group.add(checkboxPulse);
        group.add(checkboxTicks);

        this.add(cardioPan);
        this.add(checkboxBeat);
        this.add(checkboxPulse);
        this.add(checkboxTicks);
    }

    private PairValues getValue(){
        switch (selected){
            case beat:
                return dependencyCreator.getBeat(dt, f0, a);
            case pulse:
                return dependencyCreator.getPulse(dt, f0, a);
            case ticks:
                return dependencyCreator.getTicks(dt);
            default:
                return null;
        }
    }
}
