package ru.afilippo;

import javax.swing.JPanel;

public class HPF extends JPanel {
    public HPF() {
        double fcut = 180;
        int m = 16;
        double dt = 0.001;

        FiltersDependencyCreator creator = new FiltersDependencyCreator();

        GraphicPan graph = new GraphicPan("hpf", 800, 600);

        graph.setValues(creator.getHPF(fcut, m, dt));

        graph.setSmoothed();

        graph.setInputs("fcut", "m");
        graph.setInputsValues(fcut, m);

        graph.setInputCallback((defaultNumber, strings) -> creator.getHPF(
                defaultNumber.getDouble(strings[0]),
                defaultNumber.getInt(strings[1]),
                dt));

        add(graph);
    }
}
