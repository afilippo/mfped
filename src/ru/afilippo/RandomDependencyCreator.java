package ru.afilippo;

public class RandomDependencyCreator {
    RandomDependencyCreator.RandomFunction userFunction;

    RandomDependencyCreator(RandomDependencyCreator.RandomFunction userFunction){
        this.userFunction = userFunction;
    }

    public PairValues getValues(int count, double a){
        PairValues storage = new PairValues();
        double[] values = new double[count];

        double minValue = 2;
        double maxValue = -1;
        double value;
        for (int x = 0; x < count; x++){
            value = userFunction.func();
            values[x] = value;
            if (value < minValue){
                minValue = value;
            }
            if (value > maxValue){
                maxValue = value;
            }
        }

        for (int x = 0; x < count; x++) {
            value = ((values[x] - minValue) / (maxValue - minValue) - 0.5) * 2 * a;
            storage.add(x, value);
        }

        return storage;
    }

    /**
     * return a value [0;1)
     */
    interface RandomFunction{
        double func();
    }
}
