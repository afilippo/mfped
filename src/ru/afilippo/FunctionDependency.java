package ru.afilippo;

import org.jfree.data.xy.XYSeries;

class FunctionDependency extends ValueDependency{
    private UserFunction function;
    private double from;
    private double to;
    private double step;
    private double a;
    private double b;

    FunctionDependency(String title, double from, double to, double step, UserFunction userFunction){
        this.function = userFunction;
        this.title = title;

        this.from = from;
        this.to = to;
        this.step = step;
    }

    void SetParameters(double a, double b){
        this.a = a;
        this.b = b;
    }

    @Override
    PairValues getDependency(){
        series = new PairValues();

        for (double x = from; x < to; x += step){
            series.add(x, function.func(a, b, x));
        }

        return series;
    }

    interface UserFunction{
        double func(double a, double b, double x);
    }
}
