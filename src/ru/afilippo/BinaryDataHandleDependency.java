package ru.afilippo;

public class BinaryDataHandleDependency {
    private FiltersDependencyCreator filtersDependencyCreator;

    public BinaryDataHandleDependency() {
        this.filtersDependencyCreator = new FiltersDependencyCreator();
    }

    public PairValues getBSF(double fcut1, double fcut2, int m, double dt){
        return getValue(() -> filtersDependencyCreator.getBSF(fcut1, fcut2, m, dt), m, dt);
    }

    public PairValues getLPF(double fcut, int m, double dt){
        return getValue(() -> filtersDependencyCreator.getLPF(fcut, m, dt), m, dt);
    }

    public PairValues getHPF(double fcut, int m, double dt){
        return getValue(() -> filtersDependencyCreator.getHPF(fcut, m, dt), m, dt);
    }

    public PairValues getBPF(double fcut1, double fcut2, int m, double dt){
        return getValue(() -> filtersDependencyCreator.getBPF(fcut1, fcut2, m, dt), m, dt);
    }

    private PairValues getValue(FilterCalculate filterCalculate, int m, double dt){

        int N = 1000;

        PairValues fromFilter = filterCalculate.calculate();

        PairValues binaryData = BinaryFileReading.getBinaryData(dt);

        double[] yk = new double[N + 2 * m];

        for (int k = 0; k < N; k++){
            for (int l = 0; l < binaryData.size(); l++){
                if (k >= l && k - l < fromFilter.size()){
                    yk[k] += fromFilter.getY(k - l) * binaryData.getY(l);
                }
            }
        }

        PairValues output = new PairValues();
        for (int n = 0; n < N + 2 * m; n++){
            output.add(dt * n, yk[n]);
        }
        return output;
    }

    private interface FilterCalculate{
        PairValues calculate();
    }
}
