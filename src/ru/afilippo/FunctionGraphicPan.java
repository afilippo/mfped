package ru.afilippo;

public class FunctionGraphicPan extends GraphicPan{

    private double from;
    private double to;
    private double step;
    private double a;
    private double b;

    private FunctionDependencyCreator dependency;

    public FunctionGraphicPan(String title, FunctionDependencyCreator.UserFunction userFunction) {
        super(title);
        this.dependency = new FunctionDependencyCreator(userFunction);
    }

    public void setDefaultParams(double a, double b){
        this.a = a;
        this.b = b;
    }

    public void setInterval(double from, double to, double step){
        this.from = from;
        this.to = to;
        this.step = step;
    }

    public void Calculate(){
        this.setValues(this.dependency.getValues(a, b, from, to, step));
        this.setInputs("a", "b");
        this.setInputsValues(a, b);
        this.setInputCallback((defaultNumber, strings) -> {
            double a = defaultNumber.getDouble(strings[0]);
            double b = defaultNumber.getDouble(strings[1]);

            return this.dependency.getValues(a, b, from, to, step);
        });
    }
}
