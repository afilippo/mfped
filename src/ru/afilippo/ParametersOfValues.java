package ru.afilippo;

import java.util.HashMap;

public class ParametersOfValues {
    PairValues pairValues;
    Double[] pairValuesY;
    int count;
    HashMap<String, Double> cacheStore = new HashMap<>(20);

    ParametersOfValues(PairValues values){
        this.pairValues = values;
        this.pairValuesY = values.getYValues();
        this.count = values.size();
    }

    public double getMiddle() {
        if (cacheStore.containsKey("middle")){
            return cacheStore.get("middle");
        }

        double middle = 0;
        for (int i = 0; i < pairValuesY.length; i++){
            middle += pairValuesY[i];
        }
        middle = middle / count;

        cacheStore.put("middle", middle);

        return middle;
    }

    public double getMiddleSQR() {
        if (cacheStore.containsKey("middleSQR")){
            return cacheStore.get("middleSQR");
        }

        double middleSQR = 0;
        for (int i = 0; i < count; i++){
            middleSQR += Math.pow(pairValuesY[i], 2);
        }
        middleSQR = middleSQR / count;

        cacheStore.put("middleSQR", middleSQR);

        return middleSQR;
    }

    public double getMiddleSQRT(){
        if (cacheStore.containsKey("middleSQRT")){
            return cacheStore.get("middleSQRT");
        }

        double middleSQRT = Math.sqrt(getMiddleSQR());

        cacheStore.put("middleSQRT", middleSQRT);

        return middleSQRT;
    }

    public double getD(){
        if (cacheStore.containsKey("D")){
            return cacheStore.get("D");
        }

        double middle = getMiddle();

        double D = 0;
        for (int i = 0; i < count; i++){
            D += Math.pow(pairValuesY[i] - middle, 2);
        }
        D = D / count;

        cacheStore.put("D", D);

        return D;
    }

    public double getSigma(){
        if (cacheStore.containsKey("sigma")){
            return cacheStore.get("sigma");
        }

        double sigma = Math.sqrt(getD());

        cacheStore.put("sigma", sigma);

        return sigma;
    }

    public double getM3(){
        if (cacheStore.containsKey("m3")){
            return cacheStore.get("m3");
        }

        double middle = getMiddle();
        double m3 = 0;
        for (int i = 0; i < count; i++){
            m3 += Math.pow(pairValuesY[i] - middle, 3);
        }
        m3 = m3 / count;

        cacheStore.put("m3", m3);

        return m3;
    }

    public double getM4(){
        if (cacheStore.containsKey("m4")){
            return cacheStore.get("m4");
        }

        double m4 = 0;
        double middle = getMiddle();
        for (int i = 0; i < count; i++){
            m4 += Math.pow(pairValuesY[i] - middle, 4);
        }
        m4 = m4 / count;

        cacheStore.put("m4", m4);

        return m4;
    }

    public double getGamma1(){
        if (cacheStore.containsKey("gamma1")){
            return cacheStore.get("gamma1");
        }

        double gamma1 = getM3() / Math.pow(getSigma(), 3);

        cacheStore.put("gamma1", gamma1);

        return gamma1;
    }

    public double getGamma2(){
        if (cacheStore.containsKey("gamma2")){
            return cacheStore.get("gamma2");
        }

        double gamma2 = getM4() / Math.pow(getSigma(), 4) - 3;

        cacheStore.put("gamma2", gamma2);

        return gamma2;
    }
}