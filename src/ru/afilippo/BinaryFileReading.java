package ru.afilippo;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class BinaryFileReading {
    public static void main(String[] args) throws Exception {
        for (double y : getBinaryData(0).getYValues()){
            System.out.println(y);
        }

    }

    public static PairValues getBinaryData(double dt){
        PairValues pairValues = new PairValues();

        try {
            File file = new File("php.dat");
            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] number = new byte[4];
            int reading;

            int i = 0;
            while ((reading = fileInputStream.read(number)) > -1) {
                if (reading == 4) {
//                    pairValues.add(i++ * dt, fromByteArray(number));
                    pairValues.add(i++ * dt, ByteBuffer.wrap(number).order(ByteOrder.LITTLE_ENDIAN).getFloat());
                }
            }

//            File file = new File("php.dat");
//            FileInputStream fileInputStream = new FileInputStream(file);
//            byte[] number = new byte[4*1000];
//            fileInputStream.read(number);
//            ByteBuffer buffer = ByteBuffer.wrap(number);
//
//            for (int i = 0; i < 1000; i++){
//                pairValues.add(i++ * dt, buffer.getFloat(i * 4));
//            }
        } catch (Exception e){

        }
        return pairValues;
    }

    private static float fromByteArray(byte[] bytes) {
        return fromByteArray(bytes, 0);
    }

    private static float fromByteArray(byte[] bytes, int index){
        int byteNum = index * 4;
        float output = bytes[byteNum] << 24 | (bytes[byteNum+1] & 0xFF) << 16 | (bytes[byteNum+2] & 0xFF) << 8 | (bytes[byteNum+3] & 0xFF);
        long debug = bytes[byteNum] << 24 | (bytes[byteNum+1] & 0xFF) << 16 | (bytes[byteNum+2] & 0xFF) << 8 | (bytes[byteNum+3] & 0xFF);
        return output;
    }
}
