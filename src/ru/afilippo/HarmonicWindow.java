package ru.afilippo;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

import javafx.util.Pair;

public class HarmonicWindow extends JPanel{
    private GraphicPan harmonicPan;
    private JCheckBox checkboxSpike;
    private HarmonicDependencyCreator dependencyCreator;
    private boolean spikes;

    private double a;
    private double f;
    private double dt;

    HarmonicWindow(){
        spikes = false;
        a = 100;
        f = 53;
        dt = 0.001;

        dependencyCreator = new HarmonicDependencyCreator(1000);
        harmonicPan = new GraphicPan("Harmonic", 700, 430);
        harmonicPan.setValues(dependencyCreator.getValues(a, f, dt));
        harmonicPan.setInputs("A", "f", "dt");
        harmonicPan.setInputsValues(a, f, dt);
        harmonicPan.setInputCallback((defaultNumber, strings) -> {
            a = defaultNumber.getDouble(strings[0]);
            f = defaultNumber.getDouble(strings[1]);
            dt = defaultNumber.getDouble(strings[2]);
            return getDependency();
        });

        checkboxSpike = new JCheckBox("Add Spike", false);
        this.add(harmonicPan);
        this.add(checkboxSpike);

        checkboxSpike.addItemListener(itemListener -> {
            spikes = itemListener.getStateChange() == ItemEvent.SELECTED;
            harmonicPan.setValues(getDependency());
        });

    }

    private PairValues getDependency(){
        PairValues pairValues = dependencyCreator.getValues(a, f, dt);
        if (spikes){
            addSpikes(pairValues);
        }
        return pairValues;
    }

    private void addSpikes(PairValues pairValues){
        Random r = new Random();
        int size = pairValues.size();
        for (int i = 0; i < 7; i++){
            int index = r.nextInt(size);
            double mnozhitel = 5 + r.nextFloat() * 5;
            pairValues.setY(index, pairValues.getY(index) * mnozhitel);
        }
    }
}
