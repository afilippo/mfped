package ru.afilippo;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

class GraphicPan extends JPanel {

    private Graphic graphic;
//    private JButton refreshButton;
    private TextFieldWithLabel[] textFields;
//    protected ValueDependency dependency;
    private InputCallback inputCallback;

    GraphicPan(String title, PairValues values) {
        this(title);

        graphic.setValues(values);
    }

    GraphicPan(String title){
        this.setLayout(new BorderLayout());

        graphic = new Graphic(title);

        this.add(graphic, BorderLayout.CENTER);
    }

    GraphicPan(String title, int width, int height){
        this.setLayout(new BorderLayout());

        graphic = new Graphic(title, width, height);

        this.add(graphic, BorderLayout.CENTER);
    }

    protected void setValues(PairValues values){
        graphic.setValues(values);
    }

    public void setInputsValues(String... string){
        for (int i = 0, length = textFields.length; i < length; i++){
            textFields[i].setValue(string[i]);
        }
    }

    public void setInputsValues(double... values){
        for (int i = 0, length = textFields.length; i < length; i++){
            textFields[i].setValue(values[i]);
        }
    }

    void setInputs(String... inputs){

        JPanel form = new JPanel(new GridLayout(3, 1));

        textFields = new TextFieldWithLabel[inputs.length];
        int count = inputs.length;
        for (int i = 0; i < count; i++){
            textFields[i] = new TextFieldWithLabel(inputs[i], inputs[i]);
        }

        JButton refreshButton = new JButton("Refresh");

        for (TextFieldWithLabel field : textFields){
            form.add(field);
        }
        form.add(refreshButton);

        refreshButton.addActionListener((e)-> {
            String[] values = new String[count];
            for (int i = 0; i < count; i++){
                values[i] = textFields[i].getValue();
            }
            PairValues pairValues = inputCallback.setInputCallback(new DefaultNumbers(), values);
            graphic.setValues(pairValues);
        });

        this.add(form, BorderLayout.LINE_END);

    }

    void setInputCallback(InputCallback inputCallback){
        this.inputCallback = inputCallback;
    }

    void setSmoothed(){
        graphic.setSmoothed();
    }


//    abstract void setValuesFromField(double a, double b);

//    private JPanel createForm(){
//        JPanel form = new JPanel(new GridLayout(3, 1));
//
//        fieldA = new JTextField("a", 3);
//        fieldB = new JTextField("b", 3);
//
//        refreshButton = new JButton("Refresh");
//
//        refreshButton.addActionListener((e)-> {
//            double a, b;
//
//            try {
//                a = Double.parseDouble(fieldA.getBSF());
//            } catch (NumberFormatException e1){
//                a = 0;
//            }
//
//            try {
//                b = Double.parseDouble(fieldB.getBSF());
//            } catch (NumberFormatException e2){
//                b = 0;
//            }
//
//            this.setValuesFromField(a, b);
//            graphic.Calculate();
//            graphic.setValues();
//        });
//
//        form.add(fieldA);
//        form.add(fieldB);
//        form.add(refreshButton);
//
//        return form;
//    }

    interface InputCallback{
        PairValues setInputCallback(DefaultNumbers defaultNumber, String... strings);
    }

    public class DefaultNumbers {
        long getLong(String string){
            try {
                return Long.parseLong(string);
            } catch (NumberFormatException e){
                return 0;
            }
        }

        int getInt(String string){
            try {
                return Integer.parseInt(string);
            } catch (NumberFormatException e){
                return 0;
            }
        }

        double getDouble(String string){
            try {
                return Double.parseDouble(string);
            } catch (NumberFormatException e){
                return 0;
            }
        }
    }
}
