package ru.afilippo;

import javax.swing.JPanel;

public class BSF extends JPanel {
    public BSF() {
        double fcut1 = 300;
        double fcut2 = 20;
        int m = 16;
        double dt = 0.001;

        FiltersDependencyCreator creator = new FiltersDependencyCreator();

        GraphicPan graph = new GraphicPan("bsf", 800, 600);

        graph.setValues(creator.getBSF(fcut1, fcut2, m, dt));

        graph.setSmoothed();

        graph.setInputs("fcut1", "fcut2", "m");
        graph.setInputsValues(fcut1, fcut2, m);

        graph.setInputCallback((defaultNumber, strings) -> creator.getBSF(
                defaultNumber.getDouble(strings[0]),
                defaultNumber.getDouble(strings[1]),
                defaultNumber.getInt(strings[2]),
                dt));

        add(graph);
    }
}
