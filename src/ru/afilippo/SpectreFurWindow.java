package ru.afilippo;

import javax.swing.JPanel;

public class SpectreFurWindow extends JPanel {
    private GraphicPan graphicPan;
    private SpectreFurDependencyCreator dependencyCreator;

    private double a;
    private double f;
    private double dt;

    SpectreFurWindow(){
        a = 100;
        f = 53;
        dt = 0.001;

        dependencyCreator = new SpectreFurDependencyCreator(1000);
        graphicPan = new GraphicPan("spectrefur", 700, 430);
        graphicPan.setValues(dependencyCreator.getValues(a, f, dt));
        graphicPan.setInputs("A", "f", "dt");
        graphicPan.setInputsValues(a, f, dt);
        graphicPan.setInputCallback((defaultNumber, strings) -> {
            a = defaultNumber.getDouble(strings[0]);
            f = defaultNumber.getDouble(strings[1]);
            dt = defaultNumber.getDouble(strings[2]);
            return dependencyCreator.getValues(a, f, dt);
        });

        this.add(graphicPan);
    }
}
