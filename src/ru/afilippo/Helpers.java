package ru.afilippo;

public class Helpers {
    public static PairValues getSpectr(double dt, PairValues pairValues){
        PairValues outputValues = new PairValues();
        double f_gr = 1 / (2 * dt);
        int count = pairValues.size();
        double df = 1 / (count * dt);

        for (double n = 0; n < f_gr; n += df){
            double Xn;
            double Re = 0;
            for (int k = 0; k < count; k++){
                Re += pairValues.getY(k) * Math.cos(2 * Math.PI * n * k * dt);
            }
            Re = Re / count;
            double Im = 0;
            for (int k = 0; k < count; k++){
                Im += pairValues.getY(k) * Math.sin(2 * Math.PI * n * k * dt);
            }
            Im = Im / count;
            Xn = Math.sqrt(Math.pow(Re, 2) + Math.pow(Im, 2));

            outputValues.add(n, Xn);
        }

        return outputValues;
    }


}
