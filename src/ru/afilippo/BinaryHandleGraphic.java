package ru.afilippo;

import javax.swing.JPanel;

public class BinaryHandleGraphic extends JPanel {
    public BinaryHandleGraphic() {
        add(Bpf());
        add(Lpf());
        add(Hpf());
        add(Bsf());
    }

    private JPanel Bsf(){
        JPanel panel = new JPanel();

        double fcut1 = 300;
        double fcut2 = 20;
        int m = 16;
        double dt = 0.001;

        BinaryDataHandleDependency binaryDataHandleDependency = new BinaryDataHandleDependency();

        GraphicPan binaryGraph = new GraphicPan("bsf", 400, 300);

        binaryGraph.setValues(binaryDataHandleDependency.getBSF(fcut1, fcut2, m, dt));

        binaryGraph.setInputs("fcut1", "fcut2", "m");
        binaryGraph.setInputsValues(fcut1, fcut2, m);

        binaryGraph.setInputCallback((defaultNumber, strings) -> binaryDataHandleDependency.getBSF(
                defaultNumber.getDouble(strings[0]),
                defaultNumber.getDouble(strings[1]),
                defaultNumber.getInt(strings[2]),
                dt));

        panel.add(binaryGraph);

        return panel;
    }

    private JPanel Bpf(){
        JPanel panel = new JPanel();

        double fcut1 = 250;
        double fcut2 = 50;
        int m = 32;
        double dt = 0.001;

        BinaryDataHandleDependency binaryDataHandleDependency = new BinaryDataHandleDependency();

        GraphicPan binaryGraph = new GraphicPan("bpf", 400, 300);

        binaryGraph.setValues(binaryDataHandleDependency.getBPF(fcut1, fcut2, m, dt));

        binaryGraph.setInputs("fcut1", "fcut2", "m");
        binaryGraph.setInputsValues(fcut1, fcut2, m);

        binaryGraph.setInputCallback((defaultNumber, strings) -> binaryDataHandleDependency.getBPF(
                defaultNumber.getDouble(strings[0]),
                defaultNumber.getDouble(strings[1]),
                defaultNumber.getInt(strings[2]),
                dt));

        panel.add(binaryGraph);

        return panel;
    }

    private JPanel Hpf(){
        JPanel panel = new JPanel();

        double fcut = 180;
        int m = 16;
        double dt = 0.001;

        BinaryDataHandleDependency binaryDataHandleDependency = new BinaryDataHandleDependency();

        GraphicPan binaryGraph = new GraphicPan("hpf", 400, 300);

        binaryGraph.setValues(binaryDataHandleDependency.getHPF(fcut, m, dt));

        binaryGraph.setInputs("fcut", "m");
        binaryGraph.setInputsValues(fcut, m);

        binaryGraph.setInputCallback((defaultNumber, strings) -> binaryDataHandleDependency.getHPF(
                defaultNumber.getDouble(strings[0]),
                defaultNumber.getInt(strings[1]),
                dt));

        panel.add(binaryGraph);

        return panel;
    }

    private JPanel Lpf(){
        JPanel panel = new JPanel();

        double fcut = 250;
        int m = 16;
        double dt = 0.001;

        BinaryDataHandleDependency binaryDataHandleDependency = new BinaryDataHandleDependency();

        GraphicPan binaryGraph = new GraphicPan("lpf", 400, 300);

        binaryGraph.setValues(binaryDataHandleDependency.getLPF(fcut, m, dt));

        binaryGraph.setInputs("fcut", "m");
        binaryGraph.setInputsValues(fcut, m);

        binaryGraph.setInputCallback((defaultNumber, strings) -> binaryDataHandleDependency.getLPF(
                defaultNumber.getDouble(strings[0]),
                defaultNumber.getInt(strings[1]),
                dt));

        panel.add(binaryGraph);

        return panel;
    }
}
