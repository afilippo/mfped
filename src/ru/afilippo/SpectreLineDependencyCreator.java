package ru.afilippo;

public class SpectreLineDependencyCreator {
    private int N;

    public SpectreLineDependencyCreator(int n) {
        N = n;
    }

    public PairValues getValues(double a, double f, double dt){
        PairValues output = new PairValues();

        double[] massXY = new double[N];

        double[] Re = new double[N];
        double[] Im = new double[N];
        double[] Cn = new double[N];

        double[] y = new double[N];
        for (int i = 0; i < N; i++)
        {
            massXY[i] = a * i + f;


        }

        for (int n = 0; n < N; n++){
            for (int k = 0; k < N; k++){
                Re[n] += massXY[k] * Math.cos((2 * Math.PI * n * k) / N);
                Im[n] += massXY[k] * Math.sin((2 * Math.PI * n * k) / N);
            }
            Re[n] = Re[n] / N;
            Im[n] = Im[n] / N;
            Cn[n] = Math.sqrt(Math.pow(Re[n], 2) + Math.pow(Im[n], 2));
        }

        double fgr = 1 / (2 * dt);
        double df = 2 * fgr / N;
        int k1 = 0;
        for (double n = 0; n < N / 2; n += df){
            output.add(n, Cn[k1]);
            k1++;
        }
        return output;
    }
}
