package ru.afilippo;

import javax.swing.JPanel;

public class AntishiftWindow extends JPanel{
    private int N;
    private double A;
    private double f;
    private double dt;

    private GraphicPan graph1;
    private GraphicPan graph2;
    private GraphicPan graph3;

    public AntishiftWindow() {
        N = 1000;
        A = 100;
        this.f = 53;
        this.dt = 0.001;

        graph1 = new GraphicPan("antishift 1", 400, 300);
        graph1.setInputs("A", "f", "dt");
        graph1.setInputsValues(A, f, dt);
        graph1.setInputCallback((defaultNumber, strings) -> {
            A = defaultNumber.getDouble(strings[0]);
            f = defaultNumber.getDouble(strings[1]);
            dt = defaultNumber.getDouble(strings[2]);
            return calculate();
        });

        graph2 = new GraphicPan("antishift 2", 400, 300);
        graph3 = new GraphicPan("antishift 3", 400, 300);

        this.add(graph1);
        this.add(graph2);
        this.add(graph3);

        calculate();
    }

    private PairValues calculate(){
        PairValues values1 = new PairValues();
        PairValues values2 = new PairValues();
        PairValues values3 = new PairValues();
        double x;
        double[] massXY = new double[N];
        for (int i = 0; i < N; i++)
        {
            x = i * dt;
            massXY[i] = A * Math.sin(2 * Math.PI * f * x);
            values1.add(x, massXY[i]);
        }
        graph1.setValues(values1);

        double constant = (RandomGenerate.GetDouble() * 20 - 10) * 100;



        for (int i = 0; i < massXY.length; i++){
            massXY[i] = massXY[i] + constant;
            values2.add(i * dt, massXY[i]);
        }
        graph2.setValues(values2);

        double xp = 0;

        for (int i = 0; i < N; i++){
            xp = xp + massXY[i];
        }
        xp = xp / N;

        for (int i = 0; i< N;i++){
            massXY[i] = massXY[i] - xp;
            values3.add(i * dt, massXY[i]);
        }
        graph3.setValues(values3);

        return values1;
    }
}
