package ru.afilippo;

public class SpectreFurDependencyCreator {
    private int N;

    public SpectreFurDependencyCreator(int n) {
        N = n;
    }

    public PairValues getValues(double A, double f, double dt){
        PairValues output = new PairValues();

        double[] Re = new double[N];
        double[] Im = new double[N];
        double[] Cn = new double[N];

        double x;
        double[] massXY = new double[N];
        double point;
        for (int i = 0; i < N; i++)
        {
            x = i * dt;
            point = A * Math.sin(2 * Math.PI * f * x);
            massXY[i] = point;
            output.add(x, point);
        }
//        chart7.Series[0].Points.DataBindXY(dt, massXY);
        double constant = A * Math.pow(10,3);

        for (int i = 0; i < massXY.length; i++){
            massXY[i] = massXY[i] + constant;
        }

        for (int n = 0; n < N; n++){
            for (int k = 0; k < N; k++){
                Re[n] += massXY[k] * Math.cos((2 * Math.PI * n * k) / N);
                Im[n] += massXY[k] * Math.sin((2 * Math.PI * n * k) / N);
            }
            Cn[n] = Math.sqrt(Math.pow(Re[n] / N, 2) + Math.pow(Im[n] / N, 2));
        }

        double fgr = 1 / (2 * dt);
        double df = 2 * fgr / N;
        int k1 = 0;
        for (double n = 0; n < N / 2; n += df){
//            chart7.Series[0].Points.AddXY(n, Cn[k1]);
            output.add(n, Cn[k1]);
//            System.out.println(Cn[k1]);
            k1++;
        }
        return output;
    }
}
