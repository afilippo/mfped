package ru.afilippo;

import javax.swing.JPanel;

public class LPF extends JPanel {
    public LPF() {
        double fcut = 250;
        int m = 16;
        double dt = 0.001;

        FiltersDependencyCreator creator = new FiltersDependencyCreator();

        GraphicPan graph = new GraphicPan("lpf", 800, 600);

        graph.setValues(creator.getLPF(fcut, m, dt));

        graph.setSmoothed();

        graph.setInputs("fcut", "m");
        graph.setInputsValues(fcut, m);

        graph.setInputCallback((defaultNumber, strings) -> creator.getLPF(
                defaultNumber.getDouble(strings[0]),
                defaultNumber.getInt(strings[1]),
                dt));

        add(graph);
    }
}
