package ru.afilippo;

import java.util.Random;

import javax.swing.JPanel;

public class AntispikeWindow extends JPanel{
    private int N;
    private double A;
    private double f;
    private double dt;

    private GraphicPan graph1;
    private GraphicPan graph2;
    private GraphicPan graph3;

    public AntispikeWindow() {
        N = 1000;
        A = 100;
        this.f = 53;
        this.dt = 0.001;

        graph1 = new GraphicPan("antispike 1", 400, 300);
        graph1.setInputs("A", "f", "dt");
        graph1.setInputsValues(A, f, dt);
        graph1.setInputCallback((defaultNumber, strings) -> {
            A = defaultNumber.getDouble(strings[0]);
            f = defaultNumber.getDouble(strings[1]);
            dt = defaultNumber.getDouble(strings[2]);
            return calculate();
        });

        graph2 = new GraphicPan("antispike 2", 400, 300);
        graph3 = new GraphicPan("antispike 3", 400, 300);

        this.add(graph1);
        this.add(graph2);
        this.add(graph3);

        calculate();
    }

    private PairValues calculate(){
        PairValues values1 = new PairValues();

        for (int i = 0; i < N; i++){
            values1.add(i * dt, A * Math.sin(2 * Math.PI * f * i * dt));
        }
        graph1.setValues(values1);


        PairValues values2 = new PairValues(values1);
        Random r = new Random();
        int size = values2.size();
        for (int i = 0; i < 7; i++){
            int index = r.nextInt(size);
            double mnozhitel = 5 + r.nextFloat() * 5;
            values2.setY(index, values2.getY(index) * mnozhitel);
        }
        graph2.setValues(values2);

        PairValues values3 = new PairValues(values2);
        for (int i = 0; i < N-1; i++){
            if (Math.abs(values3.getY(i)) - values3.getY(i + 1) > A){
                if (Math.abs(values3.getY(i) / values3.getY(i + 1)) > 1){
                    values3.setY(i, (values3.getY(i - 1) + values3.getY(i + 1)) / 2);
                }
            }
        }
        graph3.setValues(values3);

        return values1;
    }
}
