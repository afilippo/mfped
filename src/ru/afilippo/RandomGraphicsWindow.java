package ru.afilippo;

import java.awt.GridLayout;
import java.util.Random;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import javafx.util.Pair;

class RandomGraphicsWindow extends JPanel {
    private JTextArea textArea;
    private GraphicPan gistPanel;
    private GraphicPan covariacPanel;
    private GraphicPan covariacVZPanel;

    RandomGraphicsWindow(){
        this.setLayout(new GridLayout(2,2));
        textArea = new JTextArea("");

        RandomDependencyCreator randomDependency = new RandomDependencyCreator(RandomGenerate::GetDouble);
        PairValues randomValues = randomDependency.getValues(4, 10);
        GraphicPan randomPanel = new GraphicPan("Random", randomValues);
        randomPanel.setInputs("count", "amplitude");
        randomPanel.setInputsValues("4", "10");

        randomPanel.setInputCallback((defaultNumber, strings) -> {
            int count = defaultNumber.getInt(strings[0]);
            double a = defaultNumber.getDouble(strings[1]);

            PairValues values = randomDependency.getValues(count, a);
            calculateParameters(values);
            calculateGistogramma(values, 20);
            calculateCovariac(values);
            calculateVzaimCovar(values);
            return values;
        });

        gistPanel = new GraphicPan("Gistogramma");
        gistPanel.setInputs("steps");
        calculateGistogramma(randomValues, 20);
        gistPanel.setInputsValues("20");

        covariacPanel = new GraphicPan("Covar");
        calculateCovariac(randomValues);

        covariacVZPanel = new GraphicPan("VzCov");
        calculateVzaimCovar(randomValues);

        this.add(randomPanel);
        this.add(gistPanel);
        this.add(textArea);
        this.add(covariacPanel);
        this.add(covariacVZPanel);

        calculateParameters(randomValues);
    }

    private void calculateParameters(PairValues values){

        ParametersOfValues pars = new ParametersOfValues(values);

        StringBuilder buffer = new StringBuilder("Среднее значение: ").append(pars.getMiddle()).append("\r\n");
        buffer.append("Среднее квадратное значение: ").append(pars.getMiddleSQR()).append('\n');
        buffer.append("Среднее квадратичное значение: ").append(pars.getMiddleSQRT()).append("\r\n");
        buffer.append("Дисперсия: ").append(pars.getD()).append("\r\n");
        buffer.append("Стандартное отклонение: ").append(pars.getSigma()).append("\r\n");
        buffer.append("m3: ").append(pars.getM3()).append("\r\n");
        buffer.append("m4: ").append(pars.getM4()).append("\r\n");
        buffer.append("sigma1: ").append(pars.getGamma1()).append("\r\n");
        buffer.append("sigma2: ").append(pars.getGamma2()).append("\r\n");

        textArea.setText(buffer.toString());
    }

    private void calculateGistogramma(PairValues pairValues, int steps){

        gistPanel.setValues(calculateGistValues(pairValues, steps));
        gistPanel.setInputCallback((defaultNumber, strings) -> calculateGistValues(pairValues, defaultNumber.getInt(strings[0])));
    }

    private PairValues calculateGistValues(PairValues graphValues, int steps){
        int i;
        int count = graphValues.size();

        double[] values = new double[count];

        for (i = 0; i < count; i++){
            values[i] = graphValues.getY(i);
        }

        double minValue = values[0];
        double maxValue = values[0];
        for (i = 0; i < count; i++){
            if (values[i] < minValue){
                minValue = values[i];
            }
            if (values[i] > maxValue){
                maxValue = values[i];
            }
        }

        double a = maxValue - minValue;

        int[] counts = new int[steps];
        double stepSize = 2 / (double)steps;

        double x;
        int index;
        for (i = 0; i < count; i++){
            x = values[i] / (a/2) + 1;
            index = (int)Math.floor(x / stepSize);
            if (index == counts.length){
                counts[index-1] += 1;
            } else {
                counts[index] += 1;
            }
        }

        PairValues gistValues = new PairValues();
        for (i = 0; i < steps; i++){
            x = i / ((double)(steps-1)/2) - 1;
            gistValues.add(x, counts[i]);
        }

        return gistValues;
    }

    private void calculateCovariac(PairValues pairValues){
        int N = pairValues.size();
        double[] Rxx = new double[N];

        double[] values = new double[N];

        for (int i = 0; i < N; i++){
            values[i] = pairValues.getY(i);
        }

        double middle = 0;
        for (int i = 0; i < N; i++){
            middle += values[i];
        }
        middle = middle / N;


        for (int l = 0; l < N; l++){
            Rxx[l] = 0;
            for (int k = 0; k < N - l; k++){
                Rxx[l] += (values[k] - middle) * (values[k+l] - middle);
            }
            Rxx[l] = Rxx[l]/(N);
        }
        double maxR = Rxx[0];
        for (int l = 1; l < N; l++){
            if (Rxx[l] > maxR){
                maxR = Rxx[l];
            }
        }

        PairValues outputValues = new PairValues();

        for (int l = 0; l < N; l++) {
            outputValues.add(l, Rxx[l] / maxR);
        }
        covariacPanel.setValues(outputValues);
    }

    private void calculateVzaimCovar(PairValues pairValues){
        int i;
        int count = pairValues.size();

        double[] values = new double[count];

        double middle = 0;
        for (i = 0; i < count; i++){
            middle += values[i];
        }
        middle = middle / count;

        for (i = 0; i < count; i++){
            values[i] = pairValues.getY(i);
        }

        double minValue = values[0];
        double maxValue = values[0];
        for (i = 0; i < count; i++){
            if (values[i] < minValue){
                minValue = values[i];
            }
            if (values[i] > maxValue){
                maxValue = values[i];
            }
        }

        double a = maxValue - minValue;

        Random rand = new Random();
        RandomDependencyCreator creator = new RandomDependencyCreator(rand::nextDouble);
        PairValues standartRandom = creator.getValues(count, a);

        double middleY = 0;
        for (i = 0; i < count; i++){
            middleY += standartRandom.getY(i);
        }
        middleY = middleY / count;


        double[] Rxy = new double[count];
        for (int l = 0; l < count; l++){
            Rxy[l] = 0;
            for (int k = 0; k < count - l; k++){
                Rxy[l] += (values[k] - middle) * (standartRandom.getY(k+l) - middleY);
            }
            Rxy[l] = Rxy[l]/(count);
        }

        PairValues outputValues = new PairValues();

        for (int l = 0; l < count; l++) {
            outputValues.add(l, Rxy[l]);
        }

        covariacVZPanel.setValues(outputValues);
    }
}
