package ru.afilippo;

import java.awt.event.ItemEvent;
import java.util.Random;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

public class FurieWindow extends JPanel {
    private GraphicPan harmonicPan;
    private GraphicPan spectrPan;
    private HarmonicDependencyCreator dependencyCreator;

    private double a;
    private double f;
    private double dt;

    FurieWindow(){
        a = 100;
        f = 53;
        dt = 0.001;

        dependencyCreator = new HarmonicDependencyCreator(1000);
        harmonicPan = new GraphicPan("Harmonic", 400, 300);
        harmonicPan.setInputs("A", "f", "dt");
        harmonicPan.setInputsValues(a, f, dt);
        harmonicPan.setInputCallback((defaultNumber, strings) -> {
            a = defaultNumber.getDouble(strings[0]);
            f = defaultNumber.getDouble(strings[1]);
            dt = defaultNumber.getDouble(strings[2]);
            return setValuesAndReturn();
        });

        spectrPan = new GraphicPan("Spectr", 400, 300);
        setValuesAndReturn();
        this.add(harmonicPan);
        this.add(spectrPan);

    }

    private PairValues setValuesAndReturn(){
        PairValues values = dependencyCreator.getValues(a, f, dt);
        harmonicPan.setValues(values);
        spectrPan.setValues(Helpers.getSpectr(dt, values));
        return values;
    }
}
