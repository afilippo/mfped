package ru.afilippo;

import javax.swing.JPanel;

public class BinaryGraphic extends JPanel {
    public BinaryGraphic() {
        double dt = 0.001;

        GraphicPan binaryGraph = new GraphicPan("binary data", 800, 600);
        binaryGraph.setValues(BinaryFileReading.getBinaryData(dt));
        add(binaryGraph);
    }
}
