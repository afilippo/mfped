package ru.afilippo;

import javax.swing.JPanel;

public class BPF extends JPanel {
    public BPF() {
        double fcut1 = 250;
        double fcut2 = 50;
        int m = 32;
        double dt = 0.001;

        FiltersDependencyCreator creator = new FiltersDependencyCreator();

        GraphicPan graph = new GraphicPan("bpf", 800, 600);

        graph.setValues(creator.getBPF(fcut1, fcut2, m, dt));

        graph.setSmoothed();

        graph.setInputs("fcut1", "fcut2", "m");
        graph.setInputsValues(fcut1, fcut2, m);

        graph.setInputCallback((defaultNumber, strings) -> creator.getBPF(
                defaultNumber.getDouble(strings[0]),
                defaultNumber.getDouble(strings[1]),
                defaultNumber.getInt(strings[2]),
                dt));

        add(graph);
    }
}
