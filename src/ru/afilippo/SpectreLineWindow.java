package ru.afilippo;

import javax.swing.JPanel;

public class SpectreLineWindow extends JPanel {
    private GraphicPan graphicPan;
    private SpectreLineDependencyCreator dependencyCreator;

    private double a;
    private double f;
    private double dt;

    SpectreLineWindow(){
        a = 100;
        f = 53;
        dt = 0.001;

        dependencyCreator = new SpectreLineDependencyCreator(1000);
        graphicPan = new GraphicPan("spectreline", 700, 430);
        graphicPan.setValues(dependencyCreator.getValues(a, f, dt));
        graphicPan.setInputs("A", "f", "dt");
        graphicPan.setInputsValues(a, f, dt);
        graphicPan.setInputCallback((defaultNumber, strings) -> {
            a = defaultNumber.getDouble(strings[0]);
            f = defaultNumber.getDouble(strings[1]);
            dt = defaultNumber.getDouble(strings[2]);
            return dependencyCreator.getValues(a, f, dt);
        });

        this.add(graphicPan);
    }
}
