package ru.afilippo;

import java.util.ArrayList;
import java.util.List;

public class RandomGenerate {
    public static void main(String[] s){
        int size = 100;
        List<Double> a = new ArrayList<>(size);

        for (int i = 0; i < 100; i++){
            a.add(GetDouble());
        }

        System.out.println(a);
    }

    static double GetDouble(){
        long start = System.nanoTime();
        int maxInt = Byte.MAX_VALUE;
        double random = (start % maxInt) / (double)maxInt;
        return random;
    }
}