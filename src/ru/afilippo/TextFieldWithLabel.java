package ru.afilippo;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class TextFieldWithLabel extends JPanel {
    private JTextField textField;
    private JLabel label;

    TextFieldWithLabel(String name, String value){
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        label = new JLabel(name);
        textField = new JTextField(value);
        label.setHorizontalAlignment(SwingConstants.LEFT);

        add(label);
        add(textField);
    }

    public String getValue(){
        return this.textField.getText();
    }

    public void setValue(String value){
        this.textField.setText(value);
    }

    public void setValue(int value){
        setValue(String.valueOf(value));
    }

    public void setValue(double value){
        setValue(String.valueOf(value));
    }

    TextFieldWithLabel(String name){
        this(name, "");
    }
}
