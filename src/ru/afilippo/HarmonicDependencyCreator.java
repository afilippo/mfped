package ru.afilippo;

public class HarmonicDependencyCreator{

    private double n;

    public HarmonicDependencyCreator(double n) {
        this.n = n;
    }

    public PairValues getValues(double a, double f, double dt){
        PairValues storage = new PairValues();
        for (int k = 0; k < this.n; k++){
            storage.add(dt*k, calculate(a, f, dt, k));
        }

        return storage;
    }

    public PairValues getValues(double dt,  double a0, double f0,
                                            double a1, double f1,
                                            double a2, double f2){
        PairValues storage = new PairValues();
        for (int k = 0; k < this.n; k++){
            storage.add(dt*k, calculate(a0, f0, dt, k) + calculate(a1, f1, dt, k) + calculate(a2, f2, dt, k));
        }

        return storage;
    }

    private double calculate(double a, double f, double dt, double k){
        return a * Math.sin(2 * Math.PI * f * dt * k);
    }
}
