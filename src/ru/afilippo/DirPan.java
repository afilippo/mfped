package ru.afilippo;

import java.awt.*;

import javax.swing.*;

public class DirPan extends JFrame {

    public DirPan(){
        super("Обработка и анализ экспериментальных данных");
        JButton buttonOpenFunctions = new JButton("Открыть функции");
        JButton buttonOpenRandom = new JButton("Открыть рандом");
        JButton buttonOpenHarmonic = new JButton("Открыть гармонику");
        JButton buttonOpenPolyharmonic = new JButton("Открыть полигармонику");
        JButton buttonOpenFurie = new JButton("Открыть Фурье");
        JButton buttonOpenCardio = new JButton("Открыть импульсы");
        JButton buttonOpenSpectrefur = new JButton("Открыть spectrefur");
        JButton buttonOpenSpectreline = new JButton("Открыть spectreline");
        JButton buttonOpenSpectrerand = new JButton("Открыть spectrerand");
        JButton buttonOpenAntishift = new JButton("antishift");
        JButton buttonOpenAntispike = new JButton("antispike");
        JButton buttonOpenLPF = new JButton("lpf");
        JButton buttonOpenBPF = new JButton("bpf");
        JButton buttonOpenHPF = new JButton("hpf");
        JButton buttonOpenBSF = new JButton("bsf");
        JButton buttonBinaryData = new JButton("binary data");
        JButton buttonBinaryDataHandle = new JButton("binary data handle");

        Container container = getContentPane();
        container.setLayout(new FlowLayout());
        container.add(buttonOpenFunctions);
        container.add(buttonOpenRandom);
        container.add(buttonOpenHarmonic);
        container.add(buttonOpenPolyharmonic);
        container.add(buttonOpenFurie);
        container.add(buttonOpenCardio);
        container.add(buttonOpenSpectrefur);
        container.add(buttonOpenSpectreline);
        container.add(buttonOpenSpectrerand);
        container.add(buttonOpenAntishift);
        container.add(buttonOpenAntispike);

        container.add(buttonOpenLPF);
        container.add(buttonOpenBPF);
        container.add(buttonOpenHPF);
        container.add(buttonOpenBSF);

        container.add(buttonBinaryData);
        container.add(buttonBinaryDataHandle);

        setSize(700,430);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        buttonOpenFunctions.addActionListener(e -> {
            openJPanel("Построение графика функции", new FunctionsGraphicsWindow());
        });

        buttonOpenRandom.addActionListener(e -> {
            openJPanel("Построение рандома", new RandomGraphicsWindow());
        });

        buttonOpenHarmonic.addActionListener(e -> {
            openJPanel("Построение гармоники", new HarmonicWindow());
        });

        buttonOpenPolyharmonic.addActionListener(e -> {
            openJPanel("Построение полигармоники", new PolyharmonicWindow());
        });

        buttonOpenFurie.addActionListener(e -> {
            openJPanel("Построение Фурье", new FurieWindow());
        });

        buttonOpenCardio.addActionListener(e -> {
            openJPanel("Построение имульсных", new CardioWindow());
        });

        buttonOpenSpectrefur.addActionListener(e -> {
            openJPanel("Построение specterfur", new SpectreFurWindow());
        });

        buttonOpenSpectreline.addActionListener(e -> {
            openJPanel("Построение specterline", new SpectreLineWindow());
        });

        buttonOpenSpectrerand.addActionListener(e -> {
            openJPanel("Построение specterrand", new SpectrRandWindow());
        });

        buttonOpenAntishift.addActionListener(e -> {
            openJPanel("antishift", new AntishiftWindow());
        });

        buttonOpenAntispike.addActionListener(e -> {
            openJPanel("antispike", new AntispikeWindow());
        });



        buttonOpenLPF.addActionListener(e -> {
            openJPanel("lpf", new LPF());
        });
        buttonOpenBPF.addActionListener(e -> {
            openJPanel("bpf", new BPF());
        });
        buttonOpenHPF.addActionListener(e -> {
            openJPanel("hpf", new HPF());
        });
        buttonOpenBSF.addActionListener(e -> {
            openJPanel("bsf", new BSF());
        });



        buttonBinaryData.addActionListener(e -> {
            openJPanel("binary data", new BinaryGraphic());
        });
        buttonBinaryDataHandle.addActionListener(e -> {
            openJPanel("binary data handle", new BinaryHandleGraphic());
        });
    }

    private void openJPanel(String title, JPanel panel){
        JFrame functionFrame = new JFrame(title);
        functionFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        functionFrame.setLayout(new BorderLayout());
        functionFrame.setSize(700,430);

        functionFrame.add(panel, BorderLayout.CENTER);

        functionFrame.setVisible(true);
    }

    public static void main(String[] args){
        new DirPan();
    }
}