package ru.afilippo;

public class CardioDependencyCreator {
    private int n;
    private int part;

    public CardioDependencyCreator(int n){
        this.n = n;
        this.part = n/4;
    }

    private double rnd(){
        return RandomGenerate.GetDouble();
    }

    public PairValues getTicks(double dt){
        PairValues res = new PairValues();

        for (double t = 0; t < n; t++){
            if (t % part == 0 && t != 0){
                double r = rnd() / 10;
                res.add(t * dt, 1 + r);
            } else {
                res.add(t * dt, 0);
            }
        }
        return res;
    }

    public PairValues getBeat(double dt, double f0, double alpha){
        PairValues res = new PairValues();

        for (double t = 0; t < n; t++){
            res.add(t * dt, Math.sin(2 * Math.PI * f0 * t * dt) * Math.exp(-alpha * t * dt));
        }
        return res;
    }

    public PairValues getPulse(double dt, double f0, double alpha){
        PairValues res = new PairValues();

        for (double k = 0; k < n; k += part){
            for (double t = 0; t < part; t++){
                res.add((t + k) * dt, Math.sin(2 * Math.PI * (f0 + rnd()) * t * dt) * Math.exp(-(alpha) * t * dt));
            }
        }
        return res;
    }
}
