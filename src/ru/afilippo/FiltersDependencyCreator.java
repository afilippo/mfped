package ru.afilippo;

public class FiltersDependencyCreator {

    private double[] lpf(double fcut, int m, double dt) {
        double[] d = {0.35577019, 0.2436983, 0.07211497, 0.00630165};

        double arg = 2 * fcut * dt;
        double[] lpw = new double[(2 * m) + 1];
        lpw[0] = arg;
        arg *= Math.PI;
        for (int i = 1; i <= m; i++) {
            lpw[i] = Math.sin(arg * i) / (Math.PI * i);
        }

        lpw[m] /= 2;

        double sumg = lpw[0];
        for (int i = 1; i <= m; i++) {
            double sum = d[0];
            arg = Math.PI * i / m;
            for (int k = 1; k <= 3; k++) {
                sum += 2 * d[k] * Math.cos(arg * k);
            }
            lpw[i] *= sum;
            sumg += 2 * lpw[i];
        }

        for (int i = 0; i <= m; i++) {
            lpw[i] /= sumg;
        }

        double[] Res = new double[2 * m + 1];

        for (int i = 1; i < m; i++) {
            Res[i] = lpw[m - i];
            Res[m + i] = lpw[i];
        }
        Res[m] = lpw[0];

        return Res;
    }

    private double[] hpf(double fcut, int m, double dt){
        double[] lpw = lpf(fcut, m, dt);
        double[] hpw = new double[2 * m + 1];
        for (int k = 0; k <= 2 * m; k++){
            if (k == m){
                hpw[k] = 1 - lpw[k];
            } else {
                hpw[k] = -lpw[k];
            }
        }
        return hpw;
    }

    private double[] bpf(double fcut1, double fcut2, int m, double dt){
        double[] bpw = new double[2 * m + 1];
        double[] lpw1 = lpf(fcut1, m, dt);
        double[] lpw2 = lpf(fcut2, m, dt);
        for (int i = 0; i <= 2*m; i++) {
            bpw[i] = lpw2[i] - lpw1[i];
        }
        return bpw;
    }

    private double[] bsf(double fcut1, double fcut2, int m, double dt){
        double[] lpw1 = lpf(fcut1, m, dt);
        double[] lpw2 = lpf(fcut2, m, dt);
        double[] bsw = new double[2 * m + 1];
        lpw1[m]++;
        for (int i = 0; i <= 2 * m; i++){
            bsw[i] = lpw1[i] - lpw2[i];
        }
        return bsw;
    }

    public PairValues getLPF(double fcut, int m, double dt) {
        return wrapIntoPairValues(lpf(fcut, m, dt), dt);
    }

    public PairValues getHPF(double fcut, int m, double dt) {
        return wrapIntoPairValues(hpf(fcut, m, dt), dt);
    }

    public PairValues getBPF(double fcut1, double fcut2, int m, double dt) {
        return wrapIntoPairValues(bpf(fcut1, fcut2, m, dt), dt);
    }

    public PairValues getBSF(double fcut1, double fcut2, int m, double dt) {
        return wrapIntoPairValues(bsf(fcut1, fcut2, m, dt), dt);
    }

    private PairValues wrapIntoPairValues(double[] values, double dt){
        PairValues pairValues = new PairValues();
        for (int i = 0; i < values.length; i++){
            pairValues.add(i * dt, values[i]);
        }
        return  pairValues;
    }
}