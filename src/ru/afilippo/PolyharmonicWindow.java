package ru.afilippo;

import java.awt.event.ItemEvent;
import java.util.Random;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

public class PolyharmonicWindow extends JPanel{
    private GraphicPan harmonicPan;
    private JCheckBox checkboxSpike;
    private HarmonicDependencyCreator dependencyCreator;
    private boolean spikes;

    private double dt;
    private double a0;
    private double f0;
    private double a1;
    private double f1;
    private double a2;
    private double f2;

    PolyharmonicWindow(){
        spikes = false;
        dt = 0.001;

        a0 = 100;
        f0 = 53;
        a1 = 25;
        f1 = 5;
        a2 = 30;
        f2 = 180;

        dependencyCreator = new HarmonicDependencyCreator(1000);
        harmonicPan = new GraphicPan("Polyharmonic", 700, 430);
        harmonicPan.setValues(dependencyCreator.getValues(dt, a0, f0, a1, f1, a2, f2));
        harmonicPan.setInputs("dt", "A0", "f0", "A1", "f1", "A2", "f2");
        harmonicPan.setInputsValues(dt, a0, f0, a1, f1, a2, f2);
        harmonicPan.setInputCallback((defaultNumber, strings) -> {
            dt = defaultNumber.getDouble(strings[0]);
            a0 = defaultNumber.getDouble(strings[1]);
            f0 = defaultNumber.getDouble(strings[2]);
            a1 = defaultNumber.getDouble(strings[3]);
            f1 = defaultNumber.getDouble(strings[4]);
            a2 = defaultNumber.getDouble(strings[5]);
            f2 = defaultNumber.getDouble(strings[6]);
            return getDependency();
        });

        checkboxSpike = new JCheckBox("Add Spike", false);
        this.add(harmonicPan);
        this.add(checkboxSpike);

        checkboxSpike.addItemListener(itemListener -> {
            spikes = itemListener.getStateChange() == ItemEvent.SELECTED;
            harmonicPan.setValues(getDependency());
        });

    }

    private PairValues getDependency(){
        PairValues pairValues = dependencyCreator.getValues(dt, a0, f0, a1, f1, a2, f2);
        if (spikes){
            addSpikes(pairValues);
        }
        return pairValues;
    }

    private void addSpikes(PairValues pairValues){
        Random r = new Random();
        int size = pairValues.size();
        for (int i = 0; i < 7; i++){
            int index = r.nextInt(size);
            double mnozhitel = 5 + r.nextFloat() * 5;
            pairValues.setY(index, pairValues.getY(index) * mnozhitel);
        }
    }
}
