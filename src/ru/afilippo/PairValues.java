package ru.afilippo;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jfree.data.xy.XYSeries;

public class PairValues {
    private final int INITIAL_CAPACITY = 100;

    private List<Double> xStorage;
    private List<Double> yStorage;

    PairValues(PairValues values){
        xStorage = new ArrayList<>(values.xStorage);
        yStorage = new ArrayList<>(values.yStorage);
    }

    PairValues(){
        xStorage = new ArrayList<>(INITIAL_CAPACITY);
        yStorage = new ArrayList<>(INITIAL_CAPACITY);
    }

    PairValues(int count){
        xStorage = new ArrayList<>(count);
        yStorage = new ArrayList<>(count);
    }

    void add(double x, double y){
        xStorage.add(x);
        yStorage.add(y);
    }

    int size(){
        return xStorage.size();
    }

    double getX(int index){
        return xStorage.get(index);
    }

    double getY(int index){
        return yStorage.get(index);
    }

    void setX(int index, double x){
        xStorage.set(index, x);
    }

    void setY(int index, double y){
        yStorage.set(index, y);
    }

    Double[] getYValues(){
        return yStorage.toArray(new Double[yStorage.size()]);
    }

    Double[] getXValues(){
        return xStorage.toArray(new Double[xStorage.size()]);
    }

    XYSeries convertToSeries(){
        XYSeries series = new XYSeries(true); // TODO что это за херня
        int count = this.size();
        for (int i = 0; i < count; i++) {
            series.add(this.getX(i), this.getY(i));
        }
        return series;
    }

    double getMaxY(){
        double maxY = yStorage.get(0);
        double thisY;
        for (int i = 0, size = size(); i < size; i++){
            thisY = yStorage.get(i);
            if (thisY > maxY){
                maxY = thisY;
            }
        }
        return maxY;
    }
}
